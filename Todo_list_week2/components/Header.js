import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button, Text, Modal } from 'react-native';

const Header = props => {
    return (
            <View style={styles.head}>
                 <Text style={styles.headTitle}>{props.title}</Text>
            </View>
         );
};

const styles = StyleSheet.create({
    head: { width:'100%',height:90, paddingTop:15, backgroundColor:'#f7287b',alignItems:'center', justifyContent:'center'},
    headTitle: { fontSize: 29, color: 'black'}
});
export default Header;