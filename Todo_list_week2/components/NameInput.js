import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button, Text, Modal,Alert,KeyboardAvoidingView } from 'react-native';

const NameInput = props => {
    const [enteredName, setEnteredName] = useState('');

    const nameInputHandler = (enteredText) => {
        setEnteredName(enteredText);
    };

    const addNameHandler = () => {
        if (enteredName.length === 0) {
           // Alert.alert('Invalid Text','Please enter Task',[{text:'OK',style:'destructive', onPress:nameInputHandler}]);
           return;
        }
        props.onAddName(enteredName);
        setEnteredName('');
    }
    const editNameHandler = () => {
        if (enteredName.length === 0) {
          //  Alert.alert('Invalid Text','Please enter Task',[{text:'OK',style:'destructive', onPress:nameInputHandler}]);
            return;
        }
        props.onEditName(enteredName);
        setEnteredName('');
    }

    return (
       <View style={styles.shadowpart}>
           {/* <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={30}> */}
            <TextInput placeholder="Enter Your Task" style={styles.input} onChangeText={nameInputHandler} value={enteredName} />
            <View style={styles.inputcontainer}>
                <View style={styles.button}><Button title="ADD" onPress={addNameHandler} /></View>
                <View style={styles.button}><Button title="UPDATE" onPress={editNameHandler} /></View>
            </View>
            {/* </KeyboardAvoidingView> */}
        </View> 
    );
};

const styles = StyleSheet.create({
    inputcontainer: { flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' },
    input: { borderBottomColor:'black', borderBottomWidth: 1, padding: 10, width: '90%', marginTop: 10, alignSelf:'center',fontSize:20 },
    button: { width: '25%', paddingTop: 10},
    shadowpart:{shadowColor:'black', shadowRadius:15, width:'90%',alignSelf:'center', height:120, shadowOffset:{width:8, height:8}, shadowOpacity:0.3,backgroundColor:'#DADED4',borderRadius:10, marginTop:5,elevation:10}
});
export default NameInput;