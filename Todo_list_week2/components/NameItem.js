import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Button, TextInput, CheckBox, Image } from 'react-native';
import { Entypo, AntDesign, MaterialIcons } from '@expo/vector-icons';

const NameItem = props => {
    const [isSelected, setSelection] = useState(false);
    const [markImportant, setMarkImportant] = useState(false);

    const importance = () => {
        setMarkImportant(!markImportant);
    }
    return (

        <View style={styles.listitems}>

            <Text style={styles.input}>
                <CheckBox value={isSelected} onValueChange={setSelection} style={styles.checkbox} />{isSelected ? <Text style={styles.strike}>{props.title}</Text> : markImportant ? <Text style={styles.mark}>{props.title}</Text> : <Text>{props.title}</Text>}
            </Text>
            <TouchableOpacity><Entypo name="edit" size={24} color="green" onPress={props.onEdit} /></TouchableOpacity>
            <TouchableOpacity><AntDesign name="delete" size={24} color="red" onPress={props.onDelete} /></TouchableOpacity>
            <TouchableOpacity ><MaterialIcons name="label-important" size={24} color="black" onPress={importance} /></TouchableOpacity>
        </View>
    );

};

const styles = StyleSheet.create({
    listitems: { flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', flex: 1 },
    input: { marginTop: 25, borderBottomColor: 'grey', borderBottomWidth: 1, padding: 5, width: '75%', fontSize: 20 },
    strike: { textDecorationLine: 'line-through', textDecorationStyle: 'solid', textDecorationColor: 'red' },
    checkbox: { marginRight: 5 },
    mark: { color: 'red' }
});
export default NameItem