import React, { useCallback, useState } from 'react';
import { StyleSheet, View, FlatList, ImageBackground, Button,Image } from 'react-native';
import * as Font from 'expo-font';

import NameItem from './components/NameItem';
import NameInput from './components/NameInput';
import Header from './components/Header';

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  })
}
export default function App() {
  const [courseName, setCourseName] = useState([]);
  const [taskValue, setTaskValue] = useState('');
  const [idValue, setIdValue] = useState(0);
  // const [startMode, setStartMode] = useState(false);

 // fetchFonts();

  const addNameHandler = nameTitle => {
    setCourseName(currentName => [
      ...courseName,
      { id: Math.random().toString(), value: nameTitle }
    ]);
  };

  const removeNameHandler = nameId => {
    setCourseName(currentName => {
      return currentName.filter((name) => nameId !== name.id);
    });
  }

  const idstore = (inputId, inputTask) => {
    setIdValue(inputId);
    setTaskValue(inputTask);

    setCourseName(currentName => currentName.map(item => item.id === inputId ?
      { id: item.id, value: "" } : item
    ));
  }

  const editNameHandler = (nameValue) => {
    setCourseName(currentName => currentName.map(item => item.id === idValue ?
      { id: item.id, value: nameValue } : item
    ));
    setIdValue(0);

  }

  // const showHandler = () => {
  //   setStartMode(true);
  // }

  // let content = <NameInput onAddName={addNameHandler} onEditName={editNameHandler} onShow={showHandler} />

  // if (startMode) {
  //   content = <FlatList
  //     keyExtractor={(item, index) => item.id}
  //     data={courseName} renderItem={itemData => <NameItem onDelete={removeNameHandler.bind(this, itemData.item.id)} onEdit={idstore.bind(this, itemData.item.id, itemData.item.value)} title={itemData.item.value} />}
  //   />
  // }
  return (
    <View style={styles.screen}>
      
      {/* <View style={styles.start}><Button  title="START APP" onPress={() => setStartMode(true)}/></View> */}
      <Header title="TODO LIST" />
      <ImageBackground source={require('./assets/img1.jpg')} style={styles.image}>
      <NameInput onAddName={addNameHandler} onEditName={editNameHandler} />
      <View style={styles.line} />
      {/* {content}; */}
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={courseName} renderItem={itemData => <NameItem onDelete={removeNameHandler.bind(this, itemData.item.id)} onEdit={idstore.bind(this, itemData.item.id, itemData.item.value)} title={itemData.item.value} />}
      />

      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: { backgroundColor: '#dce1e3', flex: 1 },
  line: {borderBottomColor: 'purple', borderBottomWidth: 1, marginTop: 25,width:'92%',alignSelf:'center'},
  image: {flex: 1,justifyContent: "center"}
});
