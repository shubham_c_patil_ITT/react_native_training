import React, {useState } from 'react';
import { StyleSheet, View} from 'react-native';

import TodoNavigation from './navigation/TodoNavigation';

export default function App() {
 
  return (
    <View style={styles.screen}>
      <TodoNavigation />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: { backgroundColor: '#dce1e3', flex: 1 },
  line: { borderBottomColor: 'purple', borderBottomWidth: 1, marginTop: 25, width: '92%', alignSelf: 'center' }
});
