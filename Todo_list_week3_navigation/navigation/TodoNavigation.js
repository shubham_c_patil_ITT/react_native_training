import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';


import NameInput from '../components/NameInput';
import Start from '../components/Start'
import TrashList from '../components/TrashList';
import SignIn from '../components/SignIn';
import Registration from '../components/Registration';

/* Declaring navigation screens */

const TodoNavigation = createStackNavigator({
    SignInPage: SignIn,
    RegistrationPage: Registration,
    start: Start,
    Inputs: NameInput,
   // Trash: TrashList,
});

const SignoutNavigation = createStackNavigator({
    SignOut: SignIn,
});

const MainNavigator = createDrawerNavigator({
    Home: {
        screen: TodoNavigation,
    },
    SignOut:{
        screen:SignoutNavigation,
    },
},
    {
        contentOptions: {
            activeTintColor: 'red',
            labelStyle: {
                fontFamily: 'open-sans-bold'
            }
        }
    });

export default createAppContainer(MainNavigator);