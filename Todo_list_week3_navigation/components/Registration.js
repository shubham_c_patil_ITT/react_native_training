import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button, Text, ImageBackground, Dimensions, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

/* This component is used for user registration */
const Registration = props => {
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    /* Set the state with entered text */
    const usernameHandler = (enteredText) => {
        setUserName(enteredText);
    };
    const passwordHandler = (enteredText) => {
        setPassword(enteredText);
    };
    const confirmPasswordHandler = (enteredText) => {
        setConfirmPassword(enteredText);
    };

    /* This function is used for store the userdata to Async storage and navigate to welcome/start page */
    const storeData = async () => {
        try {
            await AsyncStorage.setItem(userName, password);
            props.navigation.navigate({ routeName: 'start', params: { currentUser: userName } });
        } catch (error) {
        }
    }


    /* This function is used for retrive the userdata from Async storage and checking validation of entered data  */
    const retrieveData = async () => {
        try {
            const retdata = await AsyncStorage.getItem(userName);
            if (retdata === null && retdata !== '') {
                if (password === confirmPassword) {
                    storeData();
                }
                else {
                    Alert.alert('Invalid Text', 'Password didnt match', [{ text: 'OK', style: 'destructive' }]);
                    //  alert('Password didnt match');
                    setConfirmPassword('');
                    setPassword('');

                }
            }
            else {
                Alert.alert('Invalid Text', 'Username Present', [{ text: 'OK', style: 'destructive' }]);
                //alert('userName Present');
                setUserName('');
                setPassword('');
                setConfirmPassword('');
            }
        } catch (error) {
        }
    };


    return (
        <View style={styles.screen}>
            <ImageBackground source={require('../assets/img2.jpg')} style={styles.image}>
                <View style={styles.shadowpart}>
                    <Text style={styles.textstyle}>{'Create Your ToDo Account'}</Text>
                    <TextInput placeholder="Your Username" style={styles.input} onChangeText={usernameHandler} value={userName} />
                    <TextInput placeholder="Your Password" style={styles.input} onChangeText={passwordHandler} secureTextEntry={true} value={password} />
                    <TextInput placeholder="Confirm Your Password" style={styles.input} onChangeText={confirmPasswordHandler} secureTextEntry={true} value={confirmPassword} />

                    <View style={styles.inputcontainer}>
                        <View style={styles.button}><Button title={"Register"} onPress={retrieveData} /></View>

                    </View>
                </View>
            </ImageBackground>
        </View>
    );
};

/* Styling the registration screen */
Registration.navigationOptions = {
    headerTitle: 'Registration Page',
    headerTintColor: 'white',
    headerStyle: {
        backgroundColor: '#009B77',
    }
    
}

const styles = StyleSheet.create({
    screen: { flex: 1, textAlign: 'center' },
    inputcontainer: { flexDirection: 'column', justifyContent: 'space-evenly', alignItems: 'center' },
    input: { borderBottomColor: 'black', borderBottomWidth: 1, padding: 10, width: '80%', marginTop: 10, alignSelf: 'center', fontSize: 15 },
    button: { width: '50%', paddingTop: 20 },
    shadowpart: { shadowColor: 'black', shadowRadius: 15, width: '80%', alignSelf: 'center', height: (Dimensions.get('window').height) / 2, shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#dce1e3', borderRadius: 10, marginTop: 5, elevation: 10 },
    image: { flex: 1, justifyContent: "center" },
    textstyle: { fontSize: 20, fontWeight: "bold", fontFamily: "Cochin", paddingVertical: 10 },

});
export default Registration;