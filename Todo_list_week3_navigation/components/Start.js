import React from 'react';
import { StyleSheet, View, TextInput, Button, Text, Modal, TouchableOpacity, ImageBackground } from 'react-native';

/* This component is used for welcome page */
const Start = props => {

    /* Receive data through navigation */
    const user = props.navigation.getParam('currentUser');

    return (
        <View style={styles.head}>
            <ImageBackground source={require('../assets/img2.jpg')} style={styles.image}>
                <Text style={styles.textstyle}>Welcome {user} </Text>
                <View style={styles.button}><Button title="START APP" onPress={() => {
                    props.navigation.navigate({ routeName: 'Inputs' });
                }} />
                </View>
            </ImageBackground>
        </View>
    );
};

/* Styling the welcome screen */
Start.navigationOptions = {
    headerTitle: 'Welcome Page',
    headerTintColor: 'white',
    headerStyle: {
        backgroundColor: '#009B77',
    }
}

const styles = StyleSheet.create({
    head: { width: '100%', height: '100%', justifyContent: 'center' },
    button: { width: '35%', alignSelf: 'center' },
    image: { flex: 1, justifyContent: "center" },
    textstyle: { fontSize: 30, fontWeight: "bold", fontFamily: "Cochin",color:'indianred', paddingVertical: 20,textAlign:'center' }
});
export default Start;