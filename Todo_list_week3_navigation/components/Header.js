import React, { useState } from 'react';
import { StyleSheet, View, Text, Modal, TouchableOpacity } from 'react-native';
import {  Entypo } from '@expo/vector-icons';

/* This is the header component */
const Header = props => {
    return (
        <View style={styles.head}>
            <Text style={styles.headTitle}>TODO LIST APP</Text>
            <TouchableOpacity >< Entypo name="trash" size={28} color="black" style={styles.trash} onPress={props.onTrash}/></TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    head: { width: '100%', height: 90, backgroundColor: '#f7287b', justifyContent: 'center' },
    headTitle: { fontSize: 30, color: 'black', alignSelf: 'center' },
    trash: { alignSelf: 'flex-end', paddingRight: 30 },
});
export default Header;