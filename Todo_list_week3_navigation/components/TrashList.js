import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, CheckBox, ImageBackground, ScrollView } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const TrashList = props => {

    /* Receive data through navigation */
    const trashItems = props.navigation.getParam('deletedItems',[]);

    return (
        <View style={styles.screen}>
            <ImageBackground source={require('../assets/img1.jpg')} style={styles.image}>
                <ScrollView style={styles.screen}>
                    {
                        trashItems.map((item) => {
                            return (
                                <View style={styles.listitems}>
                                    <Text style={styles.input}>
                                        <Text>{item.value}</Text>
                                    </Text>
                                    <TouchableOpacity><MaterialCommunityIcons name="restore" size={24} color="black" color="green" onPress={props.onRestore} /></TouchableOpacity>
                                </View>
                            );
                        })
                    }
                </ScrollView>
            </ImageBackground>
        </View>
    );
};

/* Styling the Trash screen */
TrashList.navigationOptions = {
    headerTitle: 'Trash',
    headerTintColor: 'white',
    headerStyle: {
        backgroundColor: '#009B77',
    }
}


const styles = StyleSheet.create({
    listitems: { flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', flex: 1 },
    input: { marginTop: 25, borderBottomColor: 'grey', borderBottomWidth: 1, padding: 5, width: '90%', fontSize: 20 },
    image: { flex: 1, justifyContent: "center" },
    screen: { flex: 1 }
});
export default TrashList;