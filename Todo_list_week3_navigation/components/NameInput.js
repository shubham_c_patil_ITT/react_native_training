import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button, Text, Modal, Alert, KeyboardAvoidingView, ImageBackground, FlatList,Item , TouchableOpacity } from 'react-native';
//import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { Ionicons } from '@expo/vector-icons';

import Header from './Header';
import NameItem from './NameItem';
import TrashList from './TrashList';

/* This component is used to take input from user */
const NameInput = props => {
    const [enteredName, setEnteredName] = useState('');
    const [courseName, setCourseName] = useState([]);
    const [trashName, setTrashName] = useState([]);
    const [taskValue, setTaskValue] = useState('');
    const [idValue, setIdValue] = useState(0);
    const [changeButton, setChangeButton] = useState(0);

    /* It is used to delete certain task and store it in trashList */
    const removeNameHandler = (nameId, nameTask) => {
        setTrashName(currentName => [
            ...trashName,
            { id: nameId, value: nameTask }
        ]);

        setCourseName(currentName => {
            return currentName.filter((name) => nameId !== name.id);
        });
    }

    /* This function is used for set state after pressing edit button */
    const idstore = (inputId, inputTask) => {
        setIdValue(inputId);
        setTaskValue(inputTask);
        setEnteredName(inputTask);
        setChangeButton(!changeButton);
    }

    /* This function is used set entered task to given state */
    const nameInputHandler = (enteredText) => {
        setEnteredName(enteredText);
    };

    /* This function is used to add entered task to tasklist*/
    const addNameHandler = () => {
        if (enteredName.length === 0) {
             Alert.alert('Invalid Text','Please enter Task',[{text:'OK',style:'destructive', onPress:nameInputHandler}]);
            return;
        }
        setCourseName(currentName => [
            ...courseName,
            { id: Math.random().toString(), value: enteredName }
        ]);
        setEnteredName('');
    }

    /* This function is used to edit the task */
    const editNameHandler = () => {
        if (enteredName.length === 0) {
            Alert.alert('Invalid Text','Please enter Task',[{text:'OK',style:'destructive', onPress:nameInputHandler}]);
            return;
        }
        setCourseName(currentName => currentName.map(item => item.id === idValue ?
            { id: item.id, value: enteredName } : item
        ));
        setIdValue(0);
        setEnteredName('');
        setChangeButton(!changeButton);
    }

    return (
        <View style={styles.screen}>
            <Header onTrash={() => {
                props.navigation.navigate({
                    routeName: 'Trash',
                    params: { deletedItems: trashName }
                });
            }} />
            <ImageBackground source={require('../assets/img1.jpg')} style={styles.image}>
                <View style={styles.shadowpart}>
                    {/* <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={30}> */}
                    <TextInput placeholder="Enter Your Task" style={styles.input} onChangeText={nameInputHandler} value={enteredName} />

                    <View style={styles.inputcontainer}>
                        <View style={styles.button}><Button title={changeButton ? "UPDATE" : "ADD"} onPress={changeButton ? editNameHandler : addNameHandler} /></View>
                    </View>
                    {/* </KeyboardAvoidingView> */}
                </View>
                <FlatList
                    keyExtractor={(item, index) => item.id}
                    data={courseName} renderItem={itemData => <NameItem onDelete={removeNameHandler.bind(this, itemData.item.id, itemData.item.value)} onEdit={idstore.bind(this, itemData.item.id, itemData.item.value)} title={itemData.item.value} />}
                />
                {/* <TrashList onRestore={addNameHandler}/> */}
            </ImageBackground>
        </View>
    );
};

/* Used to style the header drawer button and for open-close drawer */
NameInput.navigationOptions = navData => {
    return {
        headerTitle: 'Home Page',
        headerLeft: (
            <TouchableOpacity >
                <Ionicons
                    title="Menu"
                    name="menu"
                    size={25}
                    color='white'
                    padding={10}
                    onPress={() => {
                        navData.navigation.toggleDrawer();
                    }}
                />
            </TouchableOpacity>
        ),
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: '#009B77',
        }
    }
}

    const styles = StyleSheet.create({
        screen: { backgroundColor: '#dce1e3', flex: 1 },
        inputcontainer: { flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' },
        input: { borderBottomColor: 'black', borderBottomWidth: 1, padding: 10, width: '90%', marginTop: 10, alignSelf: 'center', fontSize: 20 },
        button: { width: '19%', paddingTop: 10 },
        shadowpart: { shadowColor: 'black', shadowRadius: 15, width: '90%', alignSelf: 'center', height: 120, shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#DADED4', borderRadius: 10, marginTop: 5, elevation: 10 },
        image: { flex: 1, justifyContent: "center" }

    });
    export default NameInput;