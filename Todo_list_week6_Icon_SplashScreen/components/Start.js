import React from 'react';
import { StyleSheet, View, TextInput, Button, Text, Modal, TouchableOpacity, ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

/* This component is used for welcome page */
const Start = props => {
    /* Receive data through navigation */
    let user = props.navigation.getParam('currentUser');

    const navigatehandler = async () => {
        try {
            props.navigation.navigate({ routeName: 'Inputs' });
        }
        catch (error) {
        }
    }

    return (
        <View style={styles.head}>
            <ImageBackground source={require('../assets/img2.jpg')} style={styles.image}>
                <Text style={styles.textstyle}>Welcome {user} </Text>
                <Text style={styles.textstyle}>Organize it all with Todoist </Text>
                <View style={styles.button}><Button title="START APP" color='purple' onPress={navigatehandler} />
                </View>
            </ImageBackground>
        </View>
    );
};

/* Styling the welcome screen */
Start.navigationOptions = {
    headerTitle: 'Welcome',
    headerTintColor: 'white',
    headerStyle: {
        backgroundColor: '#009B77'
    }
}

const styles = StyleSheet.create({
    head: { width: '100%', height: '100%', justifyContent: 'center' },
    button: { width: '35%', alignSelf: 'center' },
    image: { flex: 1, justifyContent: "center" },
    textstyle: { fontSize: 30, fontWeight: "bold", color: 'black', paddingVertical: 10, textAlign: 'center' }
});
export default Start;