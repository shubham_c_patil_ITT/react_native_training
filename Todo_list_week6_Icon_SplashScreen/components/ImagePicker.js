import React from 'react';
import { View, Image, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';

/* This component is used for printing the selected image  */
const ImgPicker = props => {
    return (
        <TouchableOpacity style={styles.imagePicker} onPress={props.onDelete}>
            <Image style={styles.image} source={{ uri: props.image }} />
            <Text style={styles.textstyle} >{props.title}</Text>
            <View style={styles.lineStyle} />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    imagePicker: { alignItems: 'center' },
    image: { width: '80%', height: Dimensions.get('window').height / 4, justifyContent: 'center', alignItems: 'center' },
    textstyle: { width: '80%', fontSize: 15, textAlign: 'center' },
    lineStyle: { borderWidth: 0.5, borderColor: 'green', marginVertical: 20, width: '90%' }
});

export default ImgPicker;
