import React from 'react';
import { StyleSheet, View, Text} from 'react-native';

/* This is the header component */
const Header = props => {
    return (
        <View style={styles.head}>
            <Text style={styles.headTitle}>TODO LIST APP</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    head: { width: '100%', height: 90, justifyContent: 'center' },
    headTitle: { fontSize: 30, color: 'black', alignSelf: 'center' },
    trash: { alignSelf: 'flex-end', paddingRight: 30 },
});
export default Header;