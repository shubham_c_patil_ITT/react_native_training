import React, { useEffect, useState, useCallback } from 'react';
import { StyleSheet, View, TextInput, Button, KeyboardAvoidingView, ImageBackground, FlatList, TouchableOpacity,  ActivityIndicator} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import * as ImagePicker from 'expo-image-picker';

import { addNotes } from '../store/actions/TodoNotes';

import ImgPicker from './ImagePicker';


import * as NotesActions from '../store/actions/TodoNotes';

/* This component is used to take input from user and shows on screen */
const Notes = props => {
    const [enteredNotes, setEnteredNotes] = useState('');
    const [pickedImage, setPickedImage] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [changeButton, setChangeButton] = useState(0);

    const dispatch = useDispatch();

    /* It fetch the user Notes part */
    const loadNotes = useCallback(async () => {
        setIsLoading(true);
        await dispatch(NotesActions.fetchNotes());
        setIsLoading(false);
    }, [dispatch, setIsLoading]);

    useEffect(() => {
        loadNotes();
    }, [dispatch, loadNotes]);

    const RetData = useSelector(state => state.todosNotes.Notes);

    /* This condition checks the cotent is loading or not and shows spinner if not loaded*/
    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" />
            </View>
        );
    }

    /* This function is used set entered task to given state */
    const notesHandler = (enteredText) => {
        setEnteredNotes(enteredText);
    };

    /* It is used to delete certain note */
    const removeNotesHandler = (notesId) => {
        dispatch(NotesActions.removeNotes(notesId));
    }

    /* This function is used to add entered notes*/
    const addNotesHandler = () => {
        dispatch(addNotes(enteredNotes, pickedImage));
        setEnteredNotes('');
        setChangeButton(!changeButton);
    }

    /* This is used to select image from camera or from system */
    const takeImageHandler = async () => {
        const image = await ImagePicker.launchCameraAsync({
            allowsEditing: true,
            aspect: [16, 9],
            quality: 0.5
        });
        setPickedImage(image.uri);
        setChangeButton(!changeButton);
    };

    return (
        <View style={styles.screen}>
            <ImageBackground source={require('../assets/img1.jpg')} style={styles.image}>
                <View style={styles.shadowpart}>
                    <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={30}>
                        <TextInput placeholder="Enter Your Notes" style={styles.input} onChangeText={notesHandler} value={enteredNotes} />
                        <View style={styles.inputcontainer}>
                            <View style={styles.button}><Button title={changeButton ? "SAVE" : "ADD IMAGE"} onPress={changeButton ? addNotesHandler : takeImageHandler} /></View>
                        </View>
                    </KeyboardAvoidingView>
                </View>
                <FlatList
                    data={RetData}
                    keyExtractor={(item) => item.id}
                    renderItem={itemData => <ImgPicker image={itemData.item.Image} title={itemData.item.title} onDelete={removeNotesHandler.bind(this, itemData.item.id)} />}
                />
            </ImageBackground>
        </View>
    );
};

/* Used to style the header drawer button and for open-close drawer */
Notes.navigationOptions = navData => {
    return {
        headerTitle: 'Notes',
        headerLeft: (
            <TouchableOpacity >
                <Ionicons
                    title="Back"
                    name="arrow-back"
                    size={25}
                    color='white'
                    padding={10}
                    onPress={() => {
                        navData.navigation.navigate({
                            routeName: 'Inputs'
                        });
                    }}
                />
            </TouchableOpacity>
        ),
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: '#009B77',
        }
    }
}

const styles = StyleSheet.create({
    screen: { flex: 1 },
    inputcontainer: { flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' },
    input: { borderBottomColor: 'black', borderBottomWidth: 1, padding: 10, width: '90%', marginTop: 10, alignSelf: 'center', fontSize: 20 },
    button: { width: '45%', paddingTop: 10 },
    shadowpart: { shadowColor: 'black', shadowRadius: 15, width: '90%', alignSelf: 'center', height: 120, shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#DADED4', borderRadius: 10, marginTop: 5, elevation: 10, marginBottom: 10 },
    image: { flex: 1 }

});
export default Notes;