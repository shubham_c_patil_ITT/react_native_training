import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, View, TextInput, Button, Text, ImageBackground, Alert, TouchableOpacity, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Ionicons } from '@expo/vector-icons';
import * as UserActions from '../store/actions/Users';

import { useDispatch, useSelector } from 'react-redux';

/* This component is used for user registration */
const Registration = props => {
    const [fullname, setFullname] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [age, setAge] = useState();
    const [gender, setGender] = useState();
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const mobNoRef = useRef();
    const ageRef = useRef();
    const genderRef = useRef();
    const usernameRef = useRef();
    const passwordRef = useRef();
    const confirmpasswordRef = useRef();
    const [validate, setValidate] = useState(true);
    const [error, setError] = useState(false);

    const dispatch = useDispatch();

    /* This is used to fetch users list from firebase*/
    useEffect(() => {
        dispatch(UserActions.fetchUsers());
    }, [dispatch]);

    const RetData = useSelector(state => state.Users.UserList);

    /* set the width as device width and height as device height */
    const [availableDeviceWidth, setAvailableDeviceWidth] = useState(
        Dimensions.get('window').width
    );
    const [availableDeviceHeight, setAvailableDeviceHeight] = useState(
        Dimensions.get('window').height
    );

    useEffect(() => {
        const updateLayout = () => {
            setAvailableDeviceWidth(Dimensions.get('window').width);
            setAvailableDeviceHeight(Dimensions.get('window').height);
        };

        Dimensions.addEventListener('change', updateLayout);
        if (userName === '') {
            setValidate(true);
        };
        return () => {
            Dimensions.removeEventListener('change', updateLayout);
        };
    });

    /* Set the state with entered text */
    const fullnameHandler = (enteredText) => {
        setFullname(enteredText);
    };
    const mobileNoHandler = (enteredText) => {
        setMobileNo(enteredText);
    };
    const ageHandler = (enteredText) => {
        setAge(enteredText);
    };
    const genderHandler = (enteredText) => {
        setGender(enteredText);
    };

    /* Set the enteredtext and check for username */
    const usernameHandler = (enteredText) => {
        setError(false);
        setUserName(enteredText);
        for (const key in RetData) {
            if (RetData[key].username === enteredText) {
                setValidate(false);
            }
        }
    };
    const passwordHandler = (enteredText) => {
        setError(false);
        setPassword(enteredText);
    };
    const confirmPasswordHandler = (enteredText) => {
        setError(false);
        setConfirmPassword(enteredText);
    };


    /* This function is used for checking validation of entered data and store new users data to firebase */
    const retrieveData = async () => {
        try {
            if (validate) {
                if (password === confirmPassword) {
                    setError(false);
                    await AsyncStorage.setItem('UserName', userName);
                    await AsyncStorage.setItem('PassWord', password);
                    dispatch(UserActions.addUser(fullname, mobileNo, age, gender, userName, password));
                    props.navigation.navigate({ routeName: 'start', params: { currentUser: userName } });
                }
                else {
                    setError(true);
                    Alert.alert('Invalid Text', 'Password didnt match', [{ text: 'OK', style: 'destructive' }]);
                    setConfirmPassword('');
                    setPassword('');
                }
            }
            else {
                setError(true);
                Alert.alert('Invalid Text', 'Username Present', [{ text: 'OK', style: 'destructive' }]);
                setUserName('');
                setPassword('');
                setConfirmPassword('');
            }
        } catch (error) {
        }
    };


    return (
        <View style={styles.screen}>
            <ImageBackground source={require('../assets/img1.jpg')} style={styles.image}>
                <View style={availableDeviceHeight < 600 ? styles.shadowpart1 : availableDeviceHeight < 730 ? styles.shadowpart2 : availableDeviceHeight < 900 ? styles.shadowpart3 : styles.shadowpart4}>
                    <Text style={styles.textstyle}>{'Create Account'}</Text>
                    <TextInput placeholder="Your Name" style={styles.input} onChangeText={fullnameHandler} value={fullname} onSubmitEditing={() => { mobNoRef.current.focus(); }} />
                    <TextInput placeholder="Your Mobile No." style={styles.input} onChangeText={mobileNoHandler} value={mobileNo} onSubmitEditing={() => { ageRef.current.focus(); }} ref={mobNoRef} />
                    <TextInput placeholder="Your Age" style={styles.input} onChangeText={ageHandler} value={age} onSubmitEditing={() => { genderRef.current.focus(); }} ref={ageRef} />
                    <TextInput placeholder="Your Gender" style={styles.input} onChangeText={genderHandler} value={gender} onSubmitEditing={() => { usernameRef.current.focus(); }} ref={genderRef} />
                    <TextInput placeholder="Your Username" style={error ? styles.inputred : styles.input} onChangeText={usernameHandler} value={userName} onSubmitEditing={() => { passwordRef.current.focus(); }} ref={usernameRef} />
                    <TextInput placeholder="Your Password" style={error ? styles.inputred : styles.input} onChangeText={passwordHandler} secureTextEntry={true} value={password} onSubmitEditing={() => { confirmpasswordRef.current.focus(); }} ref={passwordRef} />
                    <TextInput placeholder="Confirm Your Password"style={error ? styles.inputred : styles.input} onChangeText={confirmPasswordHandler} secureTextEntry={true} value={confirmPassword} ref={confirmpasswordRef} />

                    <View style={styles.inputcontainer}>
                        <View style={styles.button}><Button title={"SIGN UP"} onPress={retrieveData} /></View>

                    </View>
                </View>
            </ImageBackground>
        </View>
    );
};

/* Styling the registration screen */
Registration.navigationOptions = navData => {
    return {
        headerTitle: 'Registration',
        headerTintColor: 'white',
        headerLeft: (
            <TouchableOpacity >
                <Ionicons
                    title="Back"
                    name="arrow-back"
                    size={25}
                    color='white'
                    onPress={() => {
                        navData.navigation.popToTop();
                    }}
                />
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: '#009B77'
        }
    }
}

const styles = StyleSheet.create({
    screen: { flex: 1, textAlign: 'center' },
    inputcontainer: { flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' },
    input: { borderBottomColor: 'black', borderBottomWidth: 1, padding: 10, width: '80%', marginTop: 10, alignSelf: 'center', fontSize: 15 },
    inputred: { borderColor: 'red', borderWidth: 1, padding: 10, width: '80%', marginTop: 10, alignSelf: 'center', fontSize: 15 },
    button: { width: '50%', paddingTop: 20 },
    shadowpart1: { shadowColor: 'black', shadowRadius: 15, width: '80%', alignSelf: 'center', height: '95%', shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#dce1e3', borderRadius: 10, marginTop: 5, elevation: 10 },
    shadowpart2: { shadowColor: 'black', shadowRadius: 15, width: '80%', alignSelf: 'center', height: '85%', shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#dce1e3', borderRadius: 10, marginTop: 5, elevation: 10 },
    shadowpart3: { shadowColor: 'black', shadowRadius: 15, width: '80%', alignSelf: 'center', height: '70%', shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#dce1e3', borderRadius: 10, marginTop: 5, elevation: 10 },
    shadowpart4: { shadowColor: 'black', shadowRadius: 15, width: '80%', alignSelf: 'center', height: '50%', shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#dce1e3', borderRadius: 10, marginTop: 5, elevation: 10 },
    image: { flex: 1, justifyContent: "center" },
    textstyle: { fontSize: 20, fontWeight: "bold", paddingVertical: 10 },

});
export default Registration;