import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';

import TodoReducer from './store/reducers/Todos';
import TodoNotesReducer from './store/reducers/TodoNotes';
import MainNavigator from './navigation/TodoNavigation';
import UserReducer from './store/reducers/Users';


const rootReducer = combineReducers({
  todos: TodoReducer,
  todosNotes: TodoNotesReducer,
  Users: UserReducer
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {

  return (
    <View style={styles.screen}>
      <Provider store={store}>
        <MainNavigator />
      </Provider>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: { backgroundColor: '#dce1e3', flex: 1 },
  line: { borderBottomColor: 'purple', borderBottomWidth: 1, marginTop: 25, width: '92%', alignSelf: 'center' }
});
