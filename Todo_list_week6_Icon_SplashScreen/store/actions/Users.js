export const SET_USER = 'SET_USER';
export const EDIT_USER = 'EDIT_USER';
export const ADD_USER = 'ADD_USER';
export const REMOVE_USER = 'REMOVE_USER';
import AsyncStorage from '@react-native-async-storage/async-storage';
import UserInformation from '../../models/UserInfo';


export const addUser = (name, mobileno, age, gender, username, password,profileimage) => {
    return async dispatch => {
        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/Users.json`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name, mobileno, age, gender, username, password,profileimage
            })
        });

        const resData = await response.json();
        dispatch({ type: ADD_USER, id: resData.name, name: name, mobileno: mobileno, age: age, gender: gender, username: username, password: password,profileimage:profileimage });
    };
};

export const editUser = (id, name, mobileno, age, gender, username, password,profileimage) => {
    return async dispatch => {
        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/Users/${id}.json`,
            {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name, mobileno, age, gender, username, password,profileimage
                })
            }
        );

        dispatch({
            type: EDIT_USER,
            id: id, name: name, mobileno: mobileno, age: age, gender: gender, username: username, password: password,profileimage:profileimage
        });
    };
};

export const removeUser = (id) => {
    return async dispatch => {
        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/Users/${id}.json`,
            {
                method: 'DELETE'
            }
        );
        dispatch({ type: REMOVE_USER, Id: id, });
    };
};

export const fetchUsers = () => {
    return async dispatch => {

        // any async code you want!
        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/Users.json`);
        const resData = await response.json();
        const loadedUsers = [];

        for (const key in resData) {
            loadedUsers.push(
                new UserInformation(
                    key,
                    resData[key].name,
                    resData[key].mobileno,
                    resData[key].age,
                    resData[key].gender,
                    resData[key].username,
                    resData[key].password,
                    resData[key].profileimage

                )
            );
        }
        dispatch({ type: SET_USER, users: loadedUsers });
    };
};


