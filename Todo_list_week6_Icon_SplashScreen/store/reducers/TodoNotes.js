import { ADD_NOTES, REMOVE_NOTES, SET_NOTES } from '../actions/TodoNotes';

const initialState = {
    Notes: []
};

const TodoNotesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_NOTES:
            return {
                ...state,
                Notes: [
                    ...state.Notes,
                    {
                        id: action.id,
                        title: action.title,
                        Image: action.image,
                    }
                ]
            };
        case SET_NOTES:
            return {
                Notes: action.notes,
            }
        case REMOVE_NOTES:
            return {
                ...state,
                ...state.Notes, Notes: state.Notes.filter((name) => action.Id !== name.id)
            }
        default:
            return state;
    }
};

export default TodoNotesReducer;