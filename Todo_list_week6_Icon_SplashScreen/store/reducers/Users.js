import { ADD_USER, REMOVE_USER, SET_USER,EDIT_USER } from '../actions/Users';

const initialState = {
    UserList: []
};

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_USER:
            return {
                ...state,
                UserList: [
                    ...state.UserList,
                    {
                        id : action.id,
                        name:action.name,
                        mobileno:action.mobileno, 
                        age:action.age,
                        gender:action.gender, 
                        username:action.username,
                        password:action.password,
                        profileimage:action.profileimage
                    }
                ]
            };
            case SET_USER:
            return {
                UserList: action.users,
            }
            case REMOVE_USER:
            return {
                ...state,
                ...state.UserList, UserList: state.UserList.filter((name) => action.Id !== name.id)
            }
            case EDIT_USER:
                return {
                    ...state,
                        ...state.UserList, UserList: state.UserList.map(item => item.id === action.id ? {id : action.id, name:action.name, mobileno:action.mobileno, age:action.age, gender:action.gender, username:action.username, password:action.password, profileimage:action.profileimage} : item)
                }
        default:
            return state;
    }
};

export default UserReducer;