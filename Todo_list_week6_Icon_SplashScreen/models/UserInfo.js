class UserInformation {
    constructor(id,name, mobileno,age, gender, username, password,profileimage) {
      this.id = id;
      this.name = name;
      this.mobileno = mobileno;
      this.age = age;
      this.gender = gender;
      this.username = username;
      this.password = password;
      this.profileimage = profileimage;
    }
  }
  
  export default UserInformation;
  