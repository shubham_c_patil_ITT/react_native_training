import React, { useState, useEffect, Component } from 'react';
import { StyleSheet, View, TextInput, Button, Text, Image, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
//import AsyncStorage from '@react-native-async-storage/async-storage';

import UserInformation from '../models/UserInfo'

const UserProfile = props => {
    const [isSelected, setSelection] = useState(false);
    const [fullname, setFullname] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [age, setAge] = useState();
    const [gender, setGender] = useState();
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [ismodify, setIsmodify] = useState(false);
    const [changeButton, setChangeButton] = useState(false);

    const fullnameHandler = (enteredText) => {
        setFullname(enteredText);
    };
    const mobileNoHandler = (enteredText) => {
        setMobileNo(enteredText);
    };
    const ageHandler = (enteredText) => {
        setAge(enteredText);
    };
    const genderHandler = (enteredText) => {
        setGender(enteredText);
    };
    const usernameHandler = (enteredText) => {
        setUserName(enteredText);
    };
    const passwordHandler = (enteredText) => {
        setPassword(enteredText);
    };

    const Handler = () => {
        setIsmodify(!ismodify);
        setChangeButton(!changeButton);
    };

    const profileHandler = () => {
        setFullname(fullname);
        setMobileNo(mobileNo);
        setAge(age);
        setGender(gender);
        setUserName(userName);
        setPassword(password);
        var user = new UserInformation(fullname, mobileNo, age, gender, userName, password);
        storeData(user);
        Handler();
    };

    useEffect(() => {
        retrieveData();
    }, [isSelected])

    const storeData = async (user) => {
        try {
            await AsyncStorage.setItem(userName, JSON.stringify(user));
        } catch (error) {
        }
    }

    const retrieveData = async () => {
        try {
            const username = await AsyncStorage.getItem('userKey');
            const value = await AsyncStorage.getItem(username);
            const retdata = JSON.parse(value);
            setFullname(retdata.name);
            setMobileNo(retdata.mobileno);
            setAge(retdata.age);
            setGender(retdata.gender);
            setUserName(retdata.username);
            setPassword(retdata.password);
        }
        catch (error) {
        }
    };

    return (

        <View style={styles.container}>
            <View style={styles.header}></View>
            <Image style={styles.image} source={(gender === 'Male') ? require('../assets/male.jpg') : require('../assets/female.jpg')} />
            <View style={styles.body}>
                <View style={styles.bodyContent}>
                    {ismodify ? <TextInput placeholder="Enter Your Fullname" style={styles.input} onChangeText={fullnameHandler} value={fullname} />
                        : <Text style={styles.name}>{fullname}</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your MobileNo" style={styles.input} onChangeText={mobileNoHandler} value={mobileNo} />
                        : <Text style={styles.info}>Mobile No : {mobileNo}</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your Age" style={styles.input} onChangeText={ageHandler} value={age} />
                        : <Text style={styles.info}>Age : {age} Years</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your gender" style={styles.input} onChangeText={genderHandler} value={gender} />
                        : <Text style={styles.info}>Gender : {gender}</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your UserName" style={styles.input} onChangeText={usernameHandler} value={userName} />
                        : <Text style={styles.info}>Username : {userName}</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your Password" style={styles.input} onChangeText={passwordHandler} secureTextEntry={true} value={password} />
                        : <Text style={styles.info} >Password : {password}</Text>}
                </View>
            </View>
            <TouchableOpacity style={styles.button}><Button title={changeButton ? "SAVE" : "MODIFY"} onPress={changeButton ? profileHandler : Handler} /></TouchableOpacity>
        </View>
    );
};

UserProfile.navigationOptions = {
    headerTitle: 'Profile',
    headerTintColor: 'white',
    headerStyle: {
        backgroundColor: '#009B77',
    }
}

const styles = StyleSheet.create({
    header: { backgroundColor: "#00CDCD", height: 200, },
    image: {
        width: 140, height: 140, borderRadius: 70, borderWidth: 4, borderColor: "grey", marginBottom: 10, alignSelf: 'center',
        position: 'absolute', marginTop: 130
    },
    body: { marginTop: 60 },
    bodyContent: { flex: 1, alignItems: 'center', padding: 30 },
    name: { fontSize: 28, color: "#696969", fontWeight: "bold" },
    info: { fontSize: 22, color: "#696969", marginTop: 10 },
    button: { width: '20%', paddingTop: 10, alignSelf: 'center' },
    input: { borderBottomColor: 'black', borderBottomWidth: 1, padding: 10, width: '60%', marginTop: 10, alignSelf: 'center', fontSize: 15 },
});
export default UserProfile;