import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, CheckBox, ImageBackground, ScrollView } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useSelector, useDispatch } from 'react-redux';

import { addTaskList } from '../store/actions/Todos';
import { removeTaskTrash } from '../store/actions/Todos';

const TrashList = props => {
    const dispatch = useDispatch();
    const trashItems = useSelector(state => state.todos.trashlist);

    if (trashItems.length === 0) {
        return (
            <View style={styles.screen}>
                <ImageBackground source={require('../assets/img1.jpg')} style={styles.backimage}>
                    <Text style={styles.content}>No Items Found</Text>
                </ImageBackground>
            </View>
        );
    }

    const restoreHandler = (nameId, nameTask) => {
        dispatch(removeTaskTrash(nameId, nameTask));
        dispatch(addTaskList(nameTask));
    }

    return (
        <View style={styles.screen}>
            <ImageBackground source={require('../assets/img1.jpg')} style={styles.image}>
                <ScrollView style={styles.screen}>
                    {
                        trashItems.map((item) => {
                            return (
                                <View style={styles.listitems}>
                                    <Text style={styles.input}>
                                        <Text>{item.value}</Text>
                                    </Text>
                                    <TouchableOpacity><MaterialCommunityIcons name="restore" size={24} color="black" color="green" onPress={restoreHandler.bind(this, item.id, item.value)} /></TouchableOpacity>
                                </View>
                            );
                        })
                    }
                </ScrollView>
            </ImageBackground>
        </View>
    );
};

/* Styling the Trash screen */
TrashList.navigationOptions = {
    headerTitle: 'Trash',
    headerTintColor: 'white',
    headerStyle: {
        backgroundColor: '#009B77',
    }
}

const styles = StyleSheet.create({
    listitems: { flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', flex: 1 },
    input: { marginTop: 25, borderBottomColor: 'grey', borderBottomWidth: 1, padding: 5, width: '90%', fontSize: 20 },
    image: { flex: 1, justifyContent: "center" },
    backimage: { flex: 1, justifyContent: "center", alignItems: 'center' },
    screen: { flex: 1 },
    content: { fontSize: 25, fontStyle: 'Cochin-Italic' }
});
export default TrashList;