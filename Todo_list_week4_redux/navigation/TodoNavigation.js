import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer} from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';

import NameInput from '../components/NameInput';
import Start from '../components/Start'
import TrashList from '../components/TrashList';
import SignIn from '../components/SignIn';
import Registration from '../components/Registration';
import UserProfile from '../components/UserProfile';

/* Declaring navigation screens */
const TodoNavigation = createStackNavigator({
    SignInPage: SignIn,
    RegistrationPage: Registration,
    start: Start,
    Inputs: NameInput,
    Profile: UserProfile,
    Trash: TrashList,
   
});

const ProfileNavigation = createStackNavigator({
    Profile: UserProfile,
});

const SignOutNavigation = createStackNavigator({
    SignOut: SignIn,
});

const MainNavigator = createDrawerNavigator({
    Home: {
        screen: TodoNavigation,
    },
    Profile: {
        screen: ProfileNavigation,
    },
    SignOut: {
        screen: SignOutNavigation,
    },
},
    {
        drawerBackgroundColor: 'grey',
        hideStatusBar: 'true',
        contentOptions: {
            activeTintColor: 'red',
            activeBackgroundColor: 'lightblue',
            labelStyle: {
                fontFamily: 'open-sans-bold'
            }
        }
    });

export default createAppContainer(MainNavigator);