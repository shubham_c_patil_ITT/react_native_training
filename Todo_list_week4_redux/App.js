import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';

import TodoNavigation from './navigation/TodoNavigation';
import TodoReducer from './store/reducers/Todos';


const rootReducer = combineReducers({
  todos: TodoReducer
});

const store = createStore(rootReducer);


export default function App() {

  return (
    <View style={styles.screen}>
      <Provider store={store}>
        <TodoNavigation />
      </Provider>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: { backgroundColor: '#dce1e3', flex: 1 },
  line: { borderBottomColor: 'purple', borderBottomWidth: 1, marginTop: 25, width: '92%', alignSelf: 'center' }
});
