export const ADD_TASK_TRASH = 'ADD_TASK_TRASH';
export const ADD_TASK_LIST = 'ADD_TASK_LIST';
export const EDIT_TASK_LIST = 'EDIT_TASK_LIST';
export const REMOVE_TASK_LIST = 'REMOVE_TASK_LIST';
export const REMOVE_TASK_TRASH = 'REMOVE_TASK_TRASH';



export const addTaskTrash = (id,task) => {
    return { type: ADD_TASK_TRASH, taskId : id,taskValue : task,};
};

export const removeTaskTrash = (id,task) => {
    return { type: REMOVE_TASK_TRASH,taskId : id, taskValue : task};
};

export const removeTaskList = (id,task) => {
    return { type: REMOVE_TASK_LIST,taskId : id, taskValue : task};
};

export const addTaskList = (task) => {
    return { type: ADD_TASK_LIST, taskValue : task};
};

export const editTaskList = (id,task) => {
    return { type: EDIT_TASK_LIST, taskId : id, taskValue : task};
};