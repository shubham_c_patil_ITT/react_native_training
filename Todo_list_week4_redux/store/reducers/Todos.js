import { ADD_TASK_TRASH, REMOVE_TASK_TRASH } from '../actions/Todos';
import { ADD_TASK_LIST, EDIT_TASK_LIST, REMOVE_TASK_LIST } from '../actions/Todos';

const initialState = {
    todolist: [],
    trashlist: []
};

const TodoReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TASK_TRASH:
            return {
                ...state,
                trashlist: [
                    ...state.trashlist,
                    {
                        id: action.taskId,
                        value: action.taskValue
                    }
                ]
            }

        case REMOVE_TASK_TRASH:
            return {
                ...state,
                ...state.trashlist, trashlist: state.trashlist.filter((name) => action.taskId !== name.id)
            }

        case REMOVE_TASK_LIST:
            return {
                ...state,
                ...state.todolist, todolist: state.todolist.filter((name) => action.taskId !== name.id)
            }

        case ADD_TASK_LIST:
            return {
                ...state,
                todolist: [
                    ...state.todolist,
                    {
                        id: Math.random().toString(),
                        value: action.taskValue
                    }
                ]
            }

        case EDIT_TASK_LIST:
            return {
                ...state,
                ...state.todolist, todolist: state.todolist.map(item => item.id === action.taskId ? { id: item.id, value: action.taskValue } : item)
            }
        default:
            return state;
    }
}

export default TodoReducer;