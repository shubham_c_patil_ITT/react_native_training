class UserInformation {
    constructor(name, mobileno,age, gender, username, password) {
      this.name = name;
      this.mobileno = mobileno;
      this.age = age;
      this.gender = gender;
      this.username = username;
      this.password = password;
    }
  }
  
  export default UserInformation;
  