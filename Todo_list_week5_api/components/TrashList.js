import React, { useEffect, useState, useCallback } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ImageBackground, FlatList, ActivityIndicator } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useSelector, useDispatch } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';

import { restoreTaskList } from '../store/actions/Todos';
import { removeTaskTrash } from '../store/actions/Todos';
import * as productsActions from '../store/actions/Todos';

const TrashList = props => {
    const [isLoading, setIsLoading] = useState(false);

    const dispatch = useDispatch();

    const loadTrash = useCallback(async () => {
        setIsLoading(true);
        await dispatch(productsActions.fetchTrash());
        setIsLoading(false);
    }, [dispatch, setIsLoading]);

    useEffect(() => {
        loadTrash();
    }, [dispatch, loadTrash]);
    const trashItems = useSelector(state => state.todos.trashlist);

    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" />
            </View>
        );
    }

    // if (trashItems.length === 0) {
    //     return (
    //         <View style={styles.screen}>
    //             <ImageBackground source={require('../assets/img1.jpg')} style={styles.backimage}>
    //                 <Text style={styles.content}>No Items Found</Text>
    //             </ImageBackground>
    //         </View>
    //     );
    // }

    const restoreHandler = (nameId, nameTask) => {
        // dispatch(addTaskList(nameTask));
        dispatch(restoreTaskList(nameTask));
        dispatch(removeTaskTrash(nameId, nameTask));

    }
    const removeTrashHandler = (nameId, nameTask) => {
        dispatch(removeTaskTrash(nameId, nameTask));
    }

    return (
        <View style={styles.screen}>
            <ImageBackground source={require('../assets/img1.jpg')} style={styles.image}>
                <View style={styles.screen}>
                    <FlatList
                        onRefresh={loadTrash}
                        refreshing={isLoading}
                        data={trashItems}
                        keyExtractor={(item) => item.id}
                        renderItem={itemData => <View style={styles.listitems}><TouchableOpacity style={styles.input} onPress={removeTrashHandler.bind(this, itemData.item.id, itemData.item.value)}><Text>{itemData.item.value}</Text></TouchableOpacity>
                            <TouchableOpacity><MaterialCommunityIcons name="restore" size={24} color="black" color="green" onPress={restoreHandler.bind(this, itemData.item.id, itemData.item.value)} /></TouchableOpacity></View>}
                    />
                </View>
            </ImageBackground>
        </View>
    );
};

/* Styling the Trash screen */
TrashList.navigationOptions = navData => {
    return {
        headerTitle: 'Trash',
        headerLeft: (
            <TouchableOpacity >
                <Ionicons
                    title="Back"
                    name="arrow-back"
                    size={25}
                    color='white'
                    padding={10}
                    onPress={() => {
                        navData.navigation.navigate({
                            routeName: 'Inputs'
                        });
                    }}
                />
            </TouchableOpacity>
        ),
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: '#009B77',
        }
    }
}

const styles = StyleSheet.create({
    listitems: { flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' },
    input: { marginTop: 25, borderBottomColor: 'grey', borderBottomWidth: 1, padding: 5, width: '90%', fontSize: 25 },
    image: { flex: 1, justifyContent: "center" },
    backimage: { flex: 1, justifyContent: "center", alignItems: 'center' },
    screen: { flex: 1 },
    content: { fontSize: 25 }

});
export default TrashList;