import React, { useEffect, useState,useCallback } from 'react';
import { StyleSheet, View, TextInput, Button, Alert, KeyboardAvoidingView, ImageBackground, FlatList, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';

import { addTaskTrash } from '../store/actions/Todos';
import { addTaskList } from '../store/actions/Todos';
import { editTaskList } from '../store/actions/Todos';
import { removeTaskList } from '../store/actions/Todos';
import * as productsActions from '../store/actions/Todos';

import Header from './Header';
import NameItem from './NameItem';

/* This component is used to take input from user */
const NameInput = props => {
    const [enteredName, setEnteredName] = useState('');
    const [idValue, setIdValue] = useState(0);
    const [changeButton, setChangeButton] = useState(0);
    const [isLoading, setIsLoading] = useState(false);

    const dispatch = useDispatch();

    const loadTasks = useCallback(async () => {
        setIsLoading(true);
        await dispatch(productsActions.fetchTasks());
        setIsLoading(false);
    }, [dispatch, setIsLoading]);

    useEffect(() => {
        loadTasks();
    }, [dispatch, loadTasks]);

    const taskList = useSelector(state => state.todos.todolist);

    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" />
            </View>
        );
    }

    /* It is used to delete certain task and store it in trashList */
    const removeNameHandler = (nameId, nameTask) => {
        dispatch(removeTaskList(nameId, nameTask));
        dispatch(addTaskTrash(nameTask));
    }

    /* This function is used for set state after pressing edit button */
    const idstore = (inputId, inputTask) => {
        setIdValue(inputId);
        setEnteredName(inputTask);
        setChangeButton(!changeButton);
    }

    /* This function is used set entered task to given state */
    const nameInputHandler = (enteredText) => {
        setEnteredName(enteredText);
    };

    /* This function is used to add entered task to tasklist*/
    const addNameHandler = () => {
        if (enteredName.trim().length === 0) {
            Alert.alert('Invalid Text', 'Please enter Task', [{ text: 'OK', style: 'destructive', onPress: nameInputHandler }]);
            return;
        }
        dispatch(addTaskList(enteredName));
        setEnteredName('');
    }

    /* This function is used to edit the task */
    const editNameHandler = () => {
        if (enteredName.trim().length === 0) {
            Alert.alert('Invalid Text', 'Please enter Task', [{ text: 'OK', style: 'destructive', onPress: nameInputHandler }]);
            return;
        }
        dispatch(editTaskList(idValue, enteredName));
        setIdValue(0);
        setEnteredName('');
        setChangeButton(!changeButton);
    }

    return (
        <View style={styles.screen}>
            <ImageBackground source={require('../assets/img1.jpg')} style={styles.image}>
                <Header />
                <View style={styles.shadowpart}>
                    <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={30}>
                    <TextInput placeholder="Enter Your Task" style={styles.input} onChangeText={nameInputHandler} value={enteredName} />

                    <View style={styles.inputcontainer}>
                        <View style={styles.button}><Button title={changeButton ? "UPDATE" : "ADD"} onPress={changeButton ? editNameHandler : addNameHandler} /></View>
                    </View>
                    </KeyboardAvoidingView>
                </View>
                <FlatList
                    onRefresh={loadTasks}
                    refreshing={isLoading}
                    data={taskList}
                    keyExtractor={(item) => item.id}
                    renderItem={itemData => <NameItem onDelete={removeNameHandler.bind(this, itemData.item.id, itemData.item.value)} onEdit={idstore.bind(this, itemData.item.id, itemData.item.value)} title={itemData.item.value} />}
                />
            </ImageBackground>
        </View>
    );
};

/* Used to style the header drawer button and for open-close drawer */
NameInput.navigationOptions = navData => {
    return {
        headerTitle: 'Home',
        headerLeft: (
            <TouchableOpacity >
                <Ionicons
                    title="Menu"
                    name="menu"
                    size={25}
                    color='white'
                    padding={10}
                    onPress={() => {
                        navData.navigation.toggleDrawer();
                    }}
                />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity >
                <Ionicons
                    title="Trash"
                    name="trash"
                    size={25}
                    color='white'
                    paddingRight={10}
                    onPress={() => {
                        navData.navigation.navigate({
                            routeName: 'Trash'
                        });
                    }}
                />
            </TouchableOpacity>
        ),
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: '#009B77',
        }
    }
}

const styles = StyleSheet.create({
    screen: { backgroundColor: '#dce1e3', flex: 1 },
    inputcontainer: { flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' },
    input: { borderBottomColor: 'black', borderBottomWidth: 1, padding: 10, width: '90%', marginTop: 10, alignSelf: 'center', fontSize: 20 },
    button: { width: '27%', paddingTop: 10 },
    shadowpart: { shadowColor: 'black', shadowRadius: 15, width: '90%', alignSelf: 'center', height: 120, shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#DADED4', borderRadius: 10, marginTop: 5, elevation: 10 },
    image: { flex: 1, justifyContent: "center" }

});
export default NameInput;