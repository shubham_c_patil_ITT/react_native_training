import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, View, TextInput, Button, Text, ImageBackground, Dimensions, } from 'react-native';
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';

/* This component is used for user Sign In */
const SignIn = props => {
    const [userName, setUserName] = useState('');
    const [Password, setPassword] = useState('');
    const passwordRef = useRef();

    const [availableDeviceWidth, setAvailableDeviceWidth] = useState(
        Dimensions.get('window').width
      );
      const [availableDeviceHeight, setAvailableDeviceHeight] = useState(
        Dimensions.get('window').height
      );
    
      useEffect(() => {
        const updateLayout = () => {
          setAvailableDeviceWidth(Dimensions.get('window').width);
          setAvailableDeviceHeight(Dimensions.get('window').height);
        };
    
        Dimensions.addEventListener('change', updateLayout);
    
        return () => {
          Dimensions.removeEventListener('change', updateLayout);
        };
      });

    /* Set the state with entered text */
    const usernameHandler = (enteredText) => {
        setUserName(enteredText);
    };
    const passwordHandler = (enteredText) => {
        setPassword(enteredText);
    };

    /* This function is used for retrive the userdata from Async storage and checking validation of entered data  */
    const retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem(userName)
            const retdata = JSON.parse(value);
            if (retdata.password === Password && retdata.password !== '') {
                setUserName('');
                setPassword('');
                props.navigation.navigate({ routeName: 'start', params: { currentUser: userName } });
            }
            else {
                Alert.alert('Invalid Text', 'Username/Password is wrong', [{ text: 'OK', style: 'destructive' }]);
                alert('Username/Password is wrong');
                setUserName('');
                setPassword('');
            }
        } catch (error) {
        }
    };

    return (
        <View style={styles.screen}>
            <ImageBackground source={require('../assets/img1.jpg')} style={styles.image}>
                <View style={availableDeviceHeight < 600 ? styles.shadowpart1 : availableDeviceHeight < 730 ? styles.shadowpart2 : availableDeviceHeight < 900 ? styles.shadowpart3 : styles.shadowpart4}>
                    <Text style={styles.textstyle1}>{'Sign In'} </Text>
                    <Text style={styles.textstyle2}>{'Use Your ToDo Account'}</Text>
                    <TextInput placeholder="Your Username" style={styles.input} onChangeText={usernameHandler} value={userName}
                        onSubmitEditing={() => { passwordRef.current.focus(); }} />
                    <TextInput placeholder="Your Password" style={styles.input} onChangeText={passwordHandler} secureTextEntry={true} value={Password} ref={passwordRef} />

                    <View style={styles.inputcontainer}>
                        <View style={styles.button}><Button title={"Sign In"} onPress={retrieveData} /></View>
                        <View style={styles.button}><Button title={"Create Account"} onPress={() => {
                            props.navigation.navigate({ routeName: 'RegistrationPage' });
                        }} /></View>
                    </View>
                </View>
            </ImageBackground>
        </View>
    );
};

/* Styling the Sign In screen */
SignIn.navigationOptions = {
    headerTitle: 'Sign In',
    headerTintColor: 'white',
    headerStyle: {
        backgroundColor: '#009B77'
    }
}

const styles = StyleSheet.create({
    screen: { flex: 1, textAlign: 'center' },
    inputcontainer: { flexDirection: 'column', justifyContent: 'space-evenly', alignItems: 'center' },
    input: { borderBottomColor: 'black', borderBottomWidth: 1, padding: 10, width: '80%', marginTop: 10, alignSelf: 'center', fontSize: 15 },
    button: { width: Dimensions.get('window').height < 600 ? "60%" : "50%", paddingTop: 15 },
    shadowpart1: { shadowColor: 'black', shadowRadius: 15, width: '80%', alignSelf: 'center', height: "65%", shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#dce1e3', borderRadius: 10, marginTop: 5, elevation: 10 },
    shadowpart2: { shadowColor: 'black', shadowRadius: 15, width: '80%', alignSelf: 'center', height: "55%", shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#dce1e3', borderRadius: 10, marginTop: 5, elevation: 10 },
    shadowpart3: { shadowColor: 'black', shadowRadius: 15, width: '80%', alignSelf: 'center', height: "45%", shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#dce1e3', borderRadius: 10, marginTop: 5, elevation: 10 },
    shadowpart4: { shadowColor: 'black', shadowRadius: 15, width: '80%', alignSelf: 'center', height: "30%", shadowOffset: { width: 8, height: 8 }, shadowOpacity: 0.3, backgroundColor: '#dce1e3', borderRadius: 10, marginTop: 5, elevation: 10 },
    image: { flex: 1, justifyContent: "center" },
    textstyle1: { fontSize: 30, alignSelf: 'center', fontWeight: "bold", paddingTop: 10 },
    textstyle2: { fontSize: 15,  alignSelf: 'center',color: 'black', paddingTop: 10 },

});
export default SignIn;