import React, { useState, useEffect } from 'react';
import { StyleSheet, View, TextInput, Button, Text, Image, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';

import UserInformation from '../models/UserInfo'

const UserProfile = props => {
    const [isSelected, setSelection] = useState(false);
    const [fullname, setFullname] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [age, setAge] = useState();
    const [gender, setGender] = useState();
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [ismodify, setIsmodify] = useState(false);
    const [changeButton, setChangeButton] = useState(false);

    const [availableDeviceWidth, setAvailableDeviceWidth] = useState(
        Dimensions.get('window').width
      );
      const [availableDeviceHeight, setAvailableDeviceHeight] = useState(
        Dimensions.get('window').height
      );
    
      useEffect(() => {
        const updateLayout = () => {
          setAvailableDeviceWidth(Dimensions.get('window').width);
          setAvailableDeviceHeight(Dimensions.get('window').height);
        };
    
        Dimensions.addEventListener('change', updateLayout);
    
        return () => {
          Dimensions.removeEventListener('change', updateLayout);
        };
      });

    const fullnameHandler = (enteredText) => {
        setFullname(enteredText);
    };
    const mobileNoHandler = (enteredText) => {
        setMobileNo(enteredText);
    };
    const ageHandler = (enteredText) => {
        setAge(enteredText);
    };
    const genderHandler = (enteredText) => {
        setGender(enteredText);
    };
    const usernameHandler = (enteredText) => {
        setUserName(enteredText);
    };
    const passwordHandler = (enteredText) => {
        setPassword(enteredText);
    };

    const Handler = () => {
        setIsmodify(!ismodify);
        setChangeButton(!changeButton);
    };

    const profileHandler = () => {
        setFullname(fullname);
        setMobileNo(mobileNo);
        setAge(age);
        setGender(gender);
        setUserName(userName);
        setPassword(password);
        var user = new UserInformation(fullname, mobileNo, age, gender, userName, password);
        storeData(user);
        Handler();
    };

    useEffect(() => {
        retrieveData();
    }, [isSelected])

    const storeData = async (user) => {
        try {
            await AsyncStorage.setItem(userName, JSON.stringify(user));
        } catch (error) {
        }
    }

    const retrieveData = async () => {
        try {
            const username = await AsyncStorage.getItem('userKey');
            const value = await AsyncStorage.getItem(username);
            const retdata = JSON.parse(value);
            setFullname(retdata.name);
            setMobileNo(retdata.mobileno);
            setAge(retdata.age);
            setGender(retdata.gender);
            setUserName(retdata.username);
            setPassword(retdata.password);
        }
        catch (error) {
        }
    };

    return (

        <ScrollView>
            <View style={availableDeviceHeight < 600 ? styles.header2 : styles.header1}></View>
            <Image style={availableDeviceHeight < 600 ? styles.image2 : styles.image1} source={(gender === 'Male') ? require('../assets/male.jpg') : require('../assets/female.jpg')} />
            <View style={availableDeviceHeight < 600 ? styles.body2 : styles.body1}>
                <View style={styles.bodyContent}>
                    {ismodify ? <TextInput placeholder="Enter Your Fullname" style={styles.input} onChangeText={fullnameHandler} value={fullname} />
                        : <Text style={styles.name}>{fullname}</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your MobileNo" style={styles.input} onChangeText={mobileNoHandler} value={mobileNo} />
                        : <Text style={styles.info}>Mobile No : {mobileNo}</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your Age" style={styles.input} onChangeText={ageHandler} value={age} />
                        : <Text style={styles.info}>Age : {age} Years</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your gender" style={styles.input} onChangeText={genderHandler} value={gender} />
                        : <Text style={styles.info}>Gender : {gender}</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your UserName" style={styles.input} onChangeText={usernameHandler} value={userName} />
                        : <Text style={styles.info}>Username : {userName}</Text>}
                    {ismodify ? <TextInput placeholder="Enter Your Password" style={styles.input} onChangeText={passwordHandler} secureTextEntry={true} value={password} />
                        : <Text></Text>}
                </View>
            </View>
            <TouchableOpacity style={styles.button}><Button title={changeButton ? "SAVE" : "MODIFY"} onPress={changeButton ? profileHandler : Handler} /></TouchableOpacity>
        </ScrollView>
    );
};

UserProfile.navigationOptions = navData => {
    return {
        headerTitle: 'Profile',
        headerLeft: (
            <TouchableOpacity >
                <Ionicons
                    title="Back"
                    name="arrow-back"
                    size={25}
                    color='white'
                    padding={10}
                    onPress={() => {
                        navData.navigation.navigate({
                            routeName: 'Inputs'
                        });
                    }}
                />
            </TouchableOpacity>
        ),
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: '#009B77',
        }
    }
}
const styles = StyleSheet.create({
    header1: { backgroundColor: "#00CDCD", height: 160, },
    header2: { backgroundColor: "#00CDCD", height: 110, },
    image1: {width: 130, height: 130, borderRadius: 65, borderWidth: 4, borderColor: "grey", marginBottom: 10, alignSelf: 'center',position: 'absolute', marginTop: 100},
    image2: {width: 100, height: 100, borderRadius: 50, borderWidth: 4, borderColor: "grey", marginBottom: 10, alignSelf: 'center',position: 'absolute', marginTop: 60},
    body1: { marginTop: 40 },
    body2: { marginTop: 20 },
    bodyContent: { flex: 1, alignItems: 'center', padding: 25 },
    name: { fontSize: 26, color: "#696969", fontWeight: "bold" },
    info: { fontSize: 20, color: "#696969", marginTop: 10 },
    button: { width: '25%', alignSelf: 'center' },
    input: { borderBottomColor: 'black', borderBottomWidth: 1, padding: 10, width: '60%', marginTop: 10, alignSelf: 'center', fontSize: 13 },
});
export default UserProfile;