export const SET_NOTES = 'SET_NOTES';
export const ADD_NOTES = 'ADD_NOTES';
export const REMOVE_NOTES = 'REMOVE_NOTES';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NotesInformation from '../../models/NotesInfo';


export const addNotes = (title, image) => {
  return async dispatch => {
    const username = await AsyncStorage.getItem('userKey');
    console.log(username);
    const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Notes.json`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            title,
            image
        })
    });

    const resData = await response.json();
    dispatch({ type: ADD_NOTES, title: title, image: image,id: resData.name});
  };
};

export const removeNotes = (id) => {
  return async dispatch => {
      const username = await AsyncStorage.getItem('userKey');
      const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Notes/${id}.json`,
          {
              method: 'DELETE'
          }
      );

      dispatch({
          type: REMOVE_NOTES,
          Id: id,
      });
  };
};


export const fetchNotes = () => {
  return async dispatch => {
      const username = await AsyncStorage.getItem('userKey');

      // any async code you want!
      const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Notes.json`);
      const resData = await response.json();
      const loadedNotes = [];

      for (const key in resData) {
          // console.log(resData[key].task)
          loadedNotes.push(
              new NotesInformation(
                  key,
                  resData[key].title,
                  resData[key].image,

              )
          );
      }
      dispatch({ type: SET_NOTES, notes: loadedNotes });
  };
};


