export const ADD_TASK_TRASH = 'ADD_TASK_TRASH';
export const ADD_TASK_LIST = 'ADD_TASK_LIST';
export const EDIT_TASK_LIST = 'EDIT_TASK_LIST';
export const REMOVE_TASK_LIST = 'REMOVE_TASK_LIST';
export const RESTORE_TASK_LIST = 'RESTORE_TASK_LIST';
export const REMOVE_TASK_TRASH = 'REMOVE_TASK_TRASH';
export const SET_TASKS = 'SET_TASKS';
export const SET_TRASH = 'SET_TRASH';

import TaskInformation from '../../models/TaskInfo';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const restoreTaskList = (task) => {
    return async dispatch => {
        const username = await AsyncStorage.getItem('userKey');
        console.log(username);
        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Tasks.json`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                task
            })
        });

        const resData = await response.json();

        dispatch({
            type: RESTORE_TASK_LIST,
            taskId: resData.name,
            taskValue: task,
        });
    };
};


export const addTaskTrash = (task) => {
    return async dispatch => {
        const username = await AsyncStorage.getItem('userKey');
        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Trash.json`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                task
            })
        });

        const resData = await response.json();

        dispatch({
            type: ADD_TASK_TRASH,
            taskId: resData.name,
            taskValue: task,
        });
    };
};

export const removeTaskTrash = (id) => {
    return async dispatch => {
        const username = await AsyncStorage.getItem('userKey');
        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Trash/${id}.json`,
            {
                method: 'DELETE'
            }
        );
        dispatch({
            type: REMOVE_TASK_TRASH,
            taskId: id,
        });
    };
};

export const removeTaskList = (id) => {
    return async dispatch => {
        const username = await AsyncStorage.getItem('userKey');
        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Tasks/${id}.json`,
            {
                method: 'DELETE'
            }
        );

        dispatch({
            type: REMOVE_TASK_LIST,
            taskId: id,
        });
    };
};

export const addTaskList = (task) => {
    return async dispatch => {
        const username = await AsyncStorage.getItem('userKey');
        // console.log(username);

        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Tasks.json`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                task
            })
        });

        const resData = await response.json();

        dispatch({
            type: ADD_TASK_LIST,
            taskId: resData.name,
            taskValue: task,
        });
    };
};

export const editTaskList = (id, task) => {
    return async dispatch => {
        const username = await AsyncStorage.getItem('userKey');

        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Tasks/${id}.json`,
            {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    task
                })
            }
        );

        dispatch({
            type: EDIT_TASK_LIST,
            taskId: id,
            taskValue: task
        });
    };
};


export const fetchTasks = () => {
    return async dispatch => {
        const username = await AsyncStorage.getItem('userKey');

        // any async code you want!
        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Tasks.json`);
        const resData = await response.json();
        const loadedProducts = [];

        for (const key in resData) {
            // console.log(resData[key].task)
            loadedProducts.push(
                new TaskInformation(
                    key,
                    resData[key].task
                )
            );
        }
        dispatch({ type: SET_TASKS, tasks: loadedProducts });
    };
};

export const fetchTrash = () => {
    return async dispatch => {
        const username = await AsyncStorage.getItem('userKey');

        const response = await fetch(`https://todo-list-app-7b2ae-default-rtdb.asia-southeast1.firebasedatabase.app/${username}_Trash.json`);
        const resData = await response.json();
        const loadedProducts = [];

        for (const key in resData) {
            loadedProducts.push(
                new TaskInformation(
                    key,
                    resData[key].task
                )
            );
        }
        dispatch({ type: SET_TRASH, tasks: loadedProducts });
    };
};