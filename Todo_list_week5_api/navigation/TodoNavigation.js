import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { AntDesign, FontAwesome,MaterialIcons } from '@expo/vector-icons';
import React from 'react';

import NameInput from '../components/NameInput';
import Start from '../components/Start'
import TrashList from '../components/TrashList';
import SignIn from '../components/SignIn';
import Registration from '../components/Registration';
import UserProfile from '../components/UserProfile';
import Notes from '../components/Notes';

/* Declaring navigation screens */
const TodoNavigation = createStackNavigator({
    SignInPage: SignIn,
    RegistrationPage: Registration,
    start: Start,
    //Inputs: NameInput,
   // Profile: UserProfile,
     Trash: TrashList,

});

const InputNavigation = createStackNavigator({
    Inputs: NameInput,
   
});

const ProfileNavigation = createStackNavigator({
    Profile: UserProfile,
});

const NotesNavigation = createStackNavigator({
    Notes: Notes,
});

const SignOutNavigation = createStackNavigator({
    SignOut: SignIn,
});

const SideNavigator = createDrawerNavigator({
    Home: {
        screen: InputNavigation,
        navigationOptions: {
            drawerIcon: (
                <AntDesign name="home" size={24} color="black" />
            ),
        },
    },
    Profile: {
        screen: ProfileNavigation,
        navigationOptions: {
            drawerIcon: (
                <AntDesign name="user" size={24} color="black" />
            ),
        },
    },
    Notes: {
        screen: NotesNavigation,
        navigationOptions: {
            drawerIcon: (
                <MaterialIcons name="event-note" size={24} color="black" />
            ),
        },
    },
    SignOut: {
        screen: SignOutNavigation,
        navigationOptions: {
            drawerIcon: (
                <FontAwesome name="sign-out" size={24} color="black" />
            ),
        },
    },
},
    {
        drawerBackgroundColor: 'grey',
        hideStatusBar: 'true',
        contentOptions: {
            activeTintColor: 'red',
            activeBackgroundColor: 'lightblue',
            labelStyle: {
                fontFamily: 'open-sans-bold'
            }
        }
    });


    const MainNavigator = createSwitchNavigator({
        Startup: TodoNavigation,
        Second : SideNavigator,
      });

export default createAppContainer(MainNavigator);