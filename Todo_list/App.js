import React, { useCallback, useState } from 'react';
import { StyleSheet, View, FlatList, ImageBackground } from 'react-native';

import NameItem from './components/NameItem';
import NameInput from './components/NameInput';
let ID = 0;
let Task = "";

export default function App() {
  const [courseName, setCourseName] = useState([]);
  const [inputValue, setInputValue] = useState("");

  const addNameHandler = nameTitle => {
    setCourseName(currentName => [
      ...courseName,
      { id: Math.random().toString(), value: nameTitle }
    ]);
  };

  const removeNameHandler = nameId => {
    setCourseName(currentName => {
      return currentName.filter((name) => nameId !== name.id);
    });
  }

  const idstore = (idvalue,taskvalue) =>{
    ID = idvalue;
    Task = taskvalue;
    setCourseName(currentName => currentName.map(item => item.id  === ID ? 
      {id: item.id, value :""} : item
      ));
      
  }

  const editNameHandler = (nameValue) => {
     setCourseName(currentName => currentName.map(item => item.id  === ID ? 
      {id: item.id, value :nameValue} : item
      ));
      ID = 0;
  }

  return (
    <View style={styles.screen}>
      <NameInput onAddName={addNameHandler} value={inputValue} onEditName={editNameHandler} />
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={courseName} renderItem={itemData => <NameItem onDelete={removeNameHandler.bind(this, itemData.item.id)} onEdit={idstore.bind(this, itemData.item.id,itemData.item.value)} title={itemData.item.value} />}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: { padding: 30, backgroundColor: 'azure' }
});
