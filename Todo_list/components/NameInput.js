import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button, Text } from 'react-native';

const NameInput = props => {
    const [enteredName, setEnteredName] = useState('');
    
    const nameInputHandler = (enteredText) => {
         setEnteredName(enteredText);
     };

    const addNameHandler = () => {
         if (enteredName.length == 0) {
             return;
         }
         props.onAddName(enteredName);
         setEnteredName('');
    }
    const editNameHandler = () => {
        if (enteredName.length == 0) {
            return;
        }
        props.onEditName(enteredName);
        setEnteredName('');
   }

    return (
        <View>
            <Text style={styles.title}>ToDo List</Text>
            <View style={styles.inputcontainer}>
                <TextInput placeholder="Enter Your Task" style={styles.input} onChangeText={nameInputHandler} value={enteredName} />
                <View style={styles.button}><Button title="ADD" onPress={addNameHandler} /></View>
                <View style={styles.button}><Button title="UPDATE" onPress={editNameHandler} /></View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    inputcontainer: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    input: { borderColor: 'black', borderWidth: 1, padding: 10, width: '40%' },
    button: {width: '25%', paddingTop: 10},
    title: { fontSize: 30, color: 'darkgreen', textAlign: 'center', paddingBottom: 10 }
});
export default NameInput;