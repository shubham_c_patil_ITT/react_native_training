import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Button, TextInput, CheckBox } from 'react-native';

const NameItem = props => {
    const [isSelected, setSelection] = useState(false);

    return (

        <View style={styles.listitems}>

            <Text style={styles.input}>
                <CheckBox value={isSelected} onValueChange={setSelection} style={styles.checkbox} />{isSelected ? <Text style={styles.strike}>{props.title}</Text> : <Text>{props.title}</Text>}
            </Text>

            <View style={styles.button}><Button title="EDIT" color='green' onPress={props.onEdit} /></View>
            <View style={styles.button}><Button title="DELETE" color='red' onPress={props.onDelete} /></View>

        </View>
    );

};

const styles = StyleSheet.create({
    listitems: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', flex: 1 },
    input: { backgroundColor: '#ccc', marginTop: 10, borderColor: 'black', borderWidth: 1, padding: 10, width: '40%' },
    button: { width: '25%' },
    strike: { textDecorationLine: 'line-through', textDecorationStyle: 'solid',textDecorationColor:'red' },
    checkbox: { marginRight: 5 }
});
export default NameItem